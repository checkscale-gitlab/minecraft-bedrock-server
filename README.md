# Minecraft Bedrock Server

[![docker](https://img.shields.io/badge/docker-hub-%23589ae9.svg)](https://cloud.docker.com/u/kobionic/repository/docker/kobionic/minecraft-bedrock-server)
[![pipeline status](https://gitlab.com/kobionic/gaming/minecraft-bedrock-server/badges/main/pipeline.svg)](https://gitlab.com/kobionic/gaming/minecraft-bedrock-server/pipelines)
[![license](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://gitlab.com/kobionic/gaming/minecraft-bedrock-server/blob/main/LICENSE)

Customized [Minecraft Bedrock server](https://www.minecraft.net/en-us/download/server/bedrock) image, made with ❤︎ by [KoBionic](https://gitlab.com/KoBionic).

## How to use this image

### Start a Minecraft Bedrock instance

#### With Docker

```sh
docker run --name minecraft-server -d -p 19132:19132/udp -p 19133:19133/udp kobionic/minecraft-bedrock-server
```

#### With Docker Compose

An examplary docker-compose.yml is present in the repository. Modify it according to your needs and pop an instance up by typing command below:

```sh
docker-compose up -d
```

### Configure Server

Environment variables are used to setup server at launch time. server.properties file gets overriden with variables retrieved from environment.

Read the [wiki](https://minecraft.gamepedia.com/Server.properties) if you want to know more about this file.

| variable                        |     default      | server.properties key           |
| ------------------------------- | :--------------: | ------------------------------- |
| ALLOW_CHEATS                    |      false       | allow-cheats                    |
| DEFAULT_PLAYER_PERMISSION_LEVEL |      member      | default-player-permission-level |
| DIFFICULTY                      |       easy       | difficulty                      |
| GAMEMODE                        |     survival     | gamemode                        |
| LEVEL_NAME                      |      level       | level-name                      |
| LEVEL_SEED                      |                  | level-seed                      |
| LEVEL_TYPE                      |     DEFAULT      | level-type                      |
| MAX_PLAYERS                     |        10        | max-players                     |
| MAX_THREADS                     |        8         | max-threads                     |
| ONLINE_MODE                     |       true       | online-mode                     |
| PLAYER_IDLE_TIMEOUT             |        30        | player-idle-timeout             |
| SERVER_NAME                     | Dedicated Server | server-name                     |
| SERVER_PORT                     |      19132       | server-port                     |
| SERVER_PORTV6                   |      19133       | server-portv6                   |
| TEXTUREPACK_REQUIRED            |      false       | texturepack-required            |
| TICK_DISTANCE                   |        4         | tick-distance                   |
| VIEW_DISTANCE                   |        10        | view-distance                   |
| WHITE_LIST                      |      false       | white-list                      |

Note that if an environment variable's value is changed, container must be restarted so that server reloads its settings accordingly.

### Administration Commands

Commands are available and can be used to configure some parts of the server such as permissions or players whitelist.

#### Permissions

```sh
# Will print command usage to let you know which sub-commands are available
docker exec minecraft-server permissions help
```

#### Whitelist

```sh
# Will print command usage to let you know which sub-commands are available
docker exec minecraft-server whitelist help
```

## Authors

- [**Jeremie Rodriguez**](https://gitlab.com/jeremiergz) <[jeremie.rodriguez@kobionic.com](mailto:jeremie.rodriguez@kobionic.com)> - Main developer

## License

This project is licensed under the Apache License 2.0 - see the [LICENSE](https://gitlab.com/kobionic/gaming/minecraft-bedrock-server/-/blob/main/LICENSE) file for details.
