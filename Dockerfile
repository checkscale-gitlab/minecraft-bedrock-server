FROM ubuntu:20.04 AS builder

ARG BEDROCK_SERVER_VERSION

WORKDIR /build

RUN apt-get update && apt-get install --yes curl unzip
RUN curl --compressed -fsSLo bedrock-server.zip https://minecraft.azureedge.net/bin-linux/bedrock-server-${BEDROCK_SERVER_VERSION}.zip && \
  unzip bedrock-server.zip && \
  rm bedrock-server.zip

COPY entrypoint.sh ./


FROM ubuntu:20.04

ARG BEDROCK_SERVER_VERSION
ARG VERSION

LABEL maintainer="Jeremie Rodriguez <jeremie.rodriguez@kobionic.com> (https://gitlab.com/jeremiergz)" \
  description="Customized Minecraft Bedrock server image, made with ❤︎ by KoBionic." \
  vendor="KoBionic" \
  version="${VERSION}"

ENV BEDROCK_SERVER_VERSION="${BEDROCK_SERVER_VERSION}"
ENV LD_LIBRARY_PATH="."
ENV VERSION="${VERSION}"

WORKDIR /minecraft
COPY scripts/* /usr/local/bin/
COPY --from=builder /build ./

RUN apt-get update && \
  apt-get install --yes --no-install-recommends jq libcurl4 libssl1.1 && \
  apt-get clean && \
  rm -rf /var/cache/apt/archives && \
  groupadd --gid 1000 minecraft && \
  useradd --uid 1000 --gid minecraft --system minecraft && \
  mkdir config worlds && \
  mv permissions.json whitelist.json config && \
  ln -s /minecraft/config/permissions.json /minecraft/permissions.json && \
  ln -s /minecraft/config/whitelist.json /minecraft/whitelist.json && \
  chown -R minecraft:minecraft .

USER minecraft

VOLUME /minecraft/config /minecraft/worlds

EXPOSE 19132/udp
EXPOSE 19133/udp

ENTRYPOINT [ "./entrypoint.sh" ]
